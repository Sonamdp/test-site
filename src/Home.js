import React, { Component } from "react";
import fire from "./config/Fire";
import "bulma/css/bulma.css";
import "./index.css";
import Page from "./Page";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      input: ""
    };
    this.handleChange = this.handleChange.bind(this);

    this.logout = this.logout.bind(this);
  }

  handleChange = e => {
    this.setState({
      username: e.target.value
    });
  };

  logout = () => {
    fire.auth().signOut();
  };

  render() {
    return (
      <div className="Container wrapper">
        <button className="button" onClick={this.logout}>
          Log Out
        </button>
        <h1>You are Home: {this.state.username}</h1>
        <input className="input" type="text" onChange={this.handleChange} />
        <Page />
      </div>
    );
  }
}

export default Home;
