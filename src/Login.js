import React, { Component } from "react";
import { Link } from "react-router-dom";
import fire from "./config/Fire";
import "bulma/css/bulma.css";
import "./index.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.signup = this.signup.bind(this);
    this.state = {
      email: "",
      password: ""
    };
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  login(e) {
    e.preventDefault();
    fire
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(u => {})
      .catch(error => {
        console.log(error);
      });
  }

  signup(e) {
    e.preventDefault();
    fire
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(u => {})
      .then(u => {
        console.log(u);
      })
      .catch(error => {
        console.log(error);
      });
  }
  render() {
    return (
      <div className="loginSignup">
        <form>
          <div className="field">
            <label for="exampleInputEmail1" className="label">
              Email address
            </label>
            <input
              value={this.state.email}
              onChange={this.handleChange}
              type="email"
              name="email"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              placeholder="Enter email"
              className="input"
            />
            <small id="emailHelp">
              We'll never share your email with anyone else.
            </small>
          </div>
          <div className="field">
            <label for="exampleInputPassword1" className="label">
              Password
            </label>
            <input
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
              name="password"
              id="exampleInputPassword1"
              placeholder="Password"
              className="input"
            />
          </div>
          <button
            className="button is-primary"
            type="submit"
            onClick={this.login}
          >
            Login
          </button>
          <button
            className="button is-primary"
            onClick={this.signup}
            style={{ marginLeft: "25px" }}
          >
            Signup
          </button>
        </form>
      </div>
    );
  }
}
export default Login;
