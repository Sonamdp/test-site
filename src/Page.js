import React, { Component } from "react";
import fire from "./config/Fire";
import "bulma/css/bulma.css";
import "./index.css";

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "Sonam"
    };
  }
  render() {
    return (
      <div className="Container wrapper">
        <button className="button">Click Here</button>
        <h1>You are Page: {this.state.username}</h1>
      </div>
    );
  }
}

export default Page;
